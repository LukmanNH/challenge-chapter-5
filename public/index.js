// 1. buat logic pilihan user
var userChoose = document.getElementsByClassName("user-choose");
var elementFinalResult = document.getElementById("final-result");
var imgVs = document.getElementById("img-vs");
var getScore = document.getElementById("score");

var userChooseFinal = "";
var round = 0;
var score = 0;

Array.from(userChoose).forEach(function (element) {
  element.addEventListener("click", function () {
    if (round == 0) {
      this.classList.add("active");
      userChooseFinal = this.getAttribute("data-item");
      compChooseFinal = compChoose();
      resultFinal = getFinalResult(userChooseFinal, compChooseFinal);
      getScore.innerText = `Score: ${score}`;

      // elementFinalResult.innerText = resultFinal;
      console.log("pilihan user", userChooseFinal);
      console.log("pilihan komputer", compChooseFinal);
      console.log("result final", resultFinal);
      round = 1;
    }
  });
});

// 2. buat logic pilihan komputer random
function compChoose() {
  var arrComputerChoose = ["batu", "kertas", "gunting"];
  var randomNumber = Math.floor(Math.random() * 3);
  var compChooseFinal = arrComputerChoose[randomNumber];
  var elementCompChoose = document.getElementById(`comp-${compChooseFinal}`);
  elementCompChoose.classList.add("active");
  return compChooseFinal;
}

// 3. buat logic untuk membandingkan pilihan user vs computer
function getFinalResult(userChooseFinal, compChooseFinal) {
  var resultFinal = "";
  if (userChooseFinal == "batu" && compChooseFinal == "batu") {
    resultFinal = "draw";
    imgVs.src = "assets/img/draw.png";
  }

  if (userChooseFinal == "batu" && compChooseFinal == "kertas") {
    resultFinal = "comp";
    imgVs.src = "assets/img/com_win.png";
  }

  if (userChooseFinal == "batu" && compChooseFinal == "gunting") {
    resultFinal = "user";
    imgVs.src = "assets/img/player_win.png";
    score++;
  }

  if (userChooseFinal == "kertas" && compChooseFinal == "kertas") {
    resultFinal = "draw";
    imgVs.src = "assets/img/draw.png";
  }

  if (userChooseFinal == "kertas" && compChooseFinal == "batu") {
    resultFinal = "user";
    imgVs.src = "assets/img/player_win.png";
    score++;
  }

  if (userChooseFinal == "kertas" && compChooseFinal == "gunting") {
    resultFinal = "comp";
    imgVs.src = "assets/img/com_win.png";
  }

  if (userChooseFinal == "gunting" && compChooseFinal == "gunting") {
    resultFinal = "draw";
    imgVs.src = "assets/img/draw.png";
  }

  if (userChooseFinal == "gunting" && compChooseFinal == "kertas") {
    resultFinal = "user";
    imgVs.src = "assets/img/player_win.png";
    score++;
  }

  if (userChooseFinal == "gunting" && compChooseFinal == "batu") {
    resultFinal = "comp";
    imgVs.src = "assets/img/com_win.png";
  }

  return resultFinal;
}

// 4. implement tombol refresh
const refreshElement = document.getElementsByClassName("img-refresh");
refreshElement[0].addEventListener("click", function () {
  Array.from(userChoose).forEach(function (el) {
    el.classList.remove("active");
  });
  elementFinalResult.innerText = "";
  imgVs.src = "assets/img/vs.png";
  round = 0;
});
