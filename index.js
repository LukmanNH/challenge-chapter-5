const bodyParser = require("body-parser");
const express = require("express");
const app = express();
const dataUser = require("./dataUser.json");
const fs = require("fs");

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/game", (req, res) => {
  res.render("game");
});

app.get("/register", (req, res) => {
  res.render("register");
});

app.post("/register", (req, res) => {
  const id = dataUser.length + 1;
  const username = req.body.username;
  const password = req.body.password;
  const email = req.body.email;
  const city = req.body.city;

  dataUser.push({
    id: id,
    username: username,
    password: password,
    email: email,
    city: city,
  });

  fs.writeFileSync("./dataUser.json", JSON.stringify(dataUser));

  res.render("login");
});

app.get("/login", (req, res) => {
  res.render("login");
});

app.post("/login", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  const getCredentials = dataUser.find(
    (item) => item.username == username && item.password == password
  );
  if (getCredentials) {
    res.redirect("game");
  } else {
    res.redirect("login");
  }
});

app.listen(5000, () => console.log("server berjalan di port 5000"));
